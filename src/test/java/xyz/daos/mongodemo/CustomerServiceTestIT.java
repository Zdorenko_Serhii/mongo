package xyz.daos.mongodemo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import xyz.daos.mongodemo.dao.CustomerRepository;
import xyz.daos.mongodemo.domain.Account;
import xyz.daos.mongodemo.domain.Address;
import xyz.daos.mongodemo.domain.Customer;
import xyz.daos.mongodemo.service.CustomerService;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CustomerServiceTestIT {

    @Autowired
    CustomerService sut;

    @Autowired
    CustomerRepository repository;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    void init() {
        mongoTemplate.save(createCustomer());
    }


    @AfterEach
    void cleanUp() {
        repository.deleteAll();
    }

    @Test
    void shouldSaveCustomer() {
        //given
        int sizeBefore = repository.findAll().size();

        //when
        Customer customer = createCustomer();
        mongoTemplate.save(customer);
        int sizeAfter = repository.findAll().size();

        //then
        assertEquals(sizeAfter, sizeBefore + 1);
    }


    @Test
    void shouldUpdateCustomer() {
        //given
        Customer customer = createCustomer();
        String id = mongoTemplate.save(customer).getId();
        customer.setId(id);

        //when
        customer.setFirstName("AnotherName");
        sut.updateCustomer(customer);

        //then
        Customer updated = mongoTemplate.findById(id, Customer.class);
        assertNotNull(updated);
        assertEquals(updated.getFirstName(), "AnotherName");
    }

    @Test
    void shouldFindCustomerByFullName() {
        //when
        List<Customer> found = sut.findByFirstAndLastName("FirstName", "LastName");

        //then
        assertFalse(found.isEmpty());
        assertEquals(1, found.size());
    }

    @Test
    void shouldFindCustomerByAddress() {
        //when
        List<Customer> found = sut.findByAddress(new Address("Line1", "Line2", "380"));

        //then
        assertFalse(found.isEmpty());
        assertEquals(1, found.size());
    }

    @Test
    void shouldFindCustomerByCardNumber() {
        //given
        String cardNumber = "1234 5678 9012 3456";

        //when
        Customer found = sut.findByCardNumber(cardNumber);

        //then
        assertEquals(found.getAccounts().get(0).getCardNumber(), cardNumber);
    }

    @Test
    void shouldFindCustomerWithExpiredCard() {
        //when
        List<Customer> found = sut.findCustomersWithExpiresCards();

        //then
        assertFalse(found.isEmpty());
        assertEquals(1, found.size());
    }

    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setFirstName("FirstName");
        customer.setLastName("LastName");
        customer.setEmail("email");

        Account account = new Account();
        account.setNameOnAccount("AccountName");
        account.setCardNumber("1234 5678 9012 3456");
        account.setExpirationDate(LocalDate.of(2021, 5, 31));
        customer.getAccounts().add(account);

        Address address = new Address("Line1", "Line2", "380");
        customer.getAddresses().add(address);

        return customer;
    }
}
