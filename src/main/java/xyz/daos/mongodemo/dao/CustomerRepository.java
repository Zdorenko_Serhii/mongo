package xyz.daos.mongodemo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import xyz.daos.mongodemo.domain.Address;
import xyz.daos.mongodemo.domain.Customer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    List<Customer> findCustomerByFirstNameAndLastName(String firstName, String lastName);

    Optional<Customer> findCustomersByAccountsCardNumber(String cardNumber);

    List<Customer> findCustomersByAddresses(Address address);

    List<Customer> findCustomersByAccountsExpirationDateBefore(LocalDate date);

}

