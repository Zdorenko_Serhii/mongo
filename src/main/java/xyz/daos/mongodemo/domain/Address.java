package xyz.daos.mongodemo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@AllArgsConstructor
public class Address {
    private String line1;
    private String line2;
    private String countryCode;
}
