package xyz.daos.mongodemo.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Data
@Document
public class Account {
    private String cardNumber;
    private String nameOnAccount;
    private LocalDate expirationDate;
}
