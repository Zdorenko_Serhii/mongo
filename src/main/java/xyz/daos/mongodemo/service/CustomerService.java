package xyz.daos.mongodemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.daos.mongodemo.dao.CustomerRepository;
import xyz.daos.mongodemo.domain.Address;
import xyz.daos.mongodemo.domain.Customer;

import java.time.LocalDate;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository repository;

    public void createCustomer(Customer customer) {
        repository.save(customer);
    }

    public void updateCustomer(Customer customer) {
        repository.save(customer);
    }

    public Customer findById(String id) {
        return repository.findById(id).orElseThrow() ;
    }

    public List<Customer> findByFirstAndLastName(String firstName, String lastName){
        return repository.findCustomerByFirstNameAndLastName(firstName, lastName);
    }

    public Customer findByCardNumber(String cardNumber){
        return repository.findCustomersByAccountsCardNumber(cardNumber).orElseThrow();
    }

    public List<Customer> findByAddress(Address address){
        return repository.findCustomersByAddresses(address);
    }

    public List<Customer> findCustomersWithExpiresCards(){
        return repository.findCustomersByAccountsExpirationDateBefore(LocalDate.now());
    }
}
